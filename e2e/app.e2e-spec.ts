import { GithubRepoPage } from './app.po';

describe('github-repo App', function() {
  let page: GithubRepoPage;

  beforeEach(() => {
    page = new GithubRepoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
